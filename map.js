function map(elements, cb) {
  if (!Array.isArray(elements)) {
    return [];
  }
  if (elements.length === 0) {
    return 0;
  }
  let newArray = [];
  for (let index = 0; index < elements.length; index++) {
    newArray.push(cb(elements[index]));
  }
  return newArray;
}

module.exports = map;

function filter(elements, cb) {
  if (!Array.isArray(elements)) {
    return [];
  }
  let newArray = [];
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index]) === true) {
      newArray.push(elements[index]);
    }
  }
  return newArray;
}
module.exports = filter;

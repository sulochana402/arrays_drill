function flatten(elements) {
  let newArray = [];
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      newArray.push(...flatten(elements[index]));
    } else {
      newArray.push(elements[index]);
    }
  }
  return newArray;
}
module.exports = flatten;

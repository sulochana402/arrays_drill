const find = require("../find");

const items = [1, 2, 3, 4, 5, 5];
function cb(element, index) {
  if (element > 3) {
    return element;
  }
}
let result = find(items, cb);
console.log(result);

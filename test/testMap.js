const map = require("../map");

const items = [1, 2, 3, 4, 5, 5];
function cb(element, index, items) {
  return element * element;
}
const result = map(items, cb);
console.log(result);

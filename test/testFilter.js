const filter = require("../filter");

const items = [1, 2, 3, 4, 5, 5];
function cb(element, index) {
  if (element % 2 == 0) {
    return true;
  }
  return [];
}
let result = filter(items, cb);
console.log(result);

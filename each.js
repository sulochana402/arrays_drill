function each(elements, cb) {
  if (!Array.isArray(elements)) {
    return [];
  }
  if (elements.length === 0) {
    return 0;
  }
  for (let index = 0; index < elements.length; index++) {
    cb(elements[index], index);
  }
}

module.exports = each;

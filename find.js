function find(elements, cb) {
  if (!Array.isArray(elements)) {
    return [];
  }
  if (elements.length === 0) {
    return 0;
  }
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      return elements[index];
    }
  }
  return undefined;
}
module.exports = find;
